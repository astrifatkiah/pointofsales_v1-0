<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Barang_model");
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data["Barang"] = $this->Barang_model->getAll();
        $this->load->view("admin/Barang/list", $data);
    }
	
	

    public function add()
    {
        $Barang= $this->Barang_model;
        $validation = $this->form_validation;
        $validation->set_rules($Barang->rules());

        if ($validation->run()) {
            $Barang->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $this->load->view("admin/Barang/new_form");
    }

    public function edit($ID)
    {
        if (!isset($ID)) redirect('admin/Barang');
       
        $Barang = $this->Barang_model;
        $validation = $this->form_validation;
        $validation->set_rules($Barang->rules());

        if ($validation->run()) {
            $Barang->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["Barang"] = $Barang->getById($ID);
        if (!$data["Barang"]) show_404();
        
        $this->load->view("admin/Barang/edit_form", $data);
    }

    public function delete($ID=null)
    {
        if (!isset($ID)) show_404();
        
        if ($this->Barang_model->delete($ID)) {
            redirect(site_url('admin/Barang'));
        }
    }
}