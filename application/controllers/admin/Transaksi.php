<?php
class Transaksi extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('Transaction_model');
		$this->load->model('Barang_model');
	}
	function index(){
		
			$data['data']=$this->Barang_model->tampil_barang();
			$this->load->view('admin/Transaksi/Transaction_view',$data);
		
	}
}