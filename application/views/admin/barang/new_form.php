<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/Barang/') ?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('admin/Barang/add') ?>" method="post" enctype="multipart/form-data" >
							<div class="form-group">
								<label for="name">Kode Barang*</label>
								<input class="form-control <?php echo form_error('Kode_Barang') ? 'is-invalid':'' ?>"
								 type="text" name="Kode_Barang" placeholder="Kode Barang" />
								<div class="invalid-feedback">
									<?php echo form_error('Kode_Barang') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="price">Barcode*</label>
								<input class="form-control <?php echo form_error('Barcode') ? 'is-invalid':'' ?>"
								 type="number" name="Barcode" min="0" placeholder="Barcode" />
								<div class="invalid-feedback">
									<?php echo form_error('Barcode') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="price">Nama Barang*</label>
								<input class="form-control <?php echo form_error('Nama_Barang') ? 'is-invalid':'' ?>"
								 type="number" name="Nama_Barang" min="0" placeholder="Nama Barang" />
								<div class="invalid-feedback">
									<?php echo form_error('Nama_Barang') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="price">Satuan*</label>
								<input class="form-control <?php echo form_error('Satuan') ? 'is-invalid':'' ?>"
								 type="number" name="Satuan" min="0" placeholder="Satuan" />
								<div class="invalid-feedback">
									<?php echo form_error('Satuan') ?>
								</div>
							</div>
							
							<div class="form-group">
								<label for="price">Harga*</label>
								<input class="form-control <?php echo form_error('Harga') ? 'is-invalid':'' ?>"
								 type="number" name="Harga" min="0" placeholder="Harga" />
								<div class="invalid-feedback">
									<?php echo form_error('Harga') ?>
								</div>
							</div>

							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>

		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>