<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model
{
    private $_table = "barang";

    public $ID;
    public $Kode_Barang;
    public $Barcode;
    public $Nama_Barang;
    public $Satuan;
	public $Harga;
	public $Created_Date;
	public $Update_Date;

    public function rules()
    {
        return [
            ['field' => 'Kode_Barang',
            'label' => 'Kode Barang',
            'rules' => 'required'],

            ['field' => 'Barcode',
            'label' => 'Barcode',
            'rules' => 'required'],
            
            ['field' => 'Nama_Barang',
            'label' => 'Nama Barang',
            'rules' => 'required'],
			
			['field' => 'Satuan',
            'label' => 'Satuan',
            'rules' => 'required'],
			
			['field' => 'Harga',
            'label' => 'Harga',
            'rules' => 'numeric'],
			
			['field' => 'Created_Date',
	        'label' => 'Created Date'],
            
			['field' => 'Update_Date',
            'label' => 'Update_Date']
            
        ];
    }
	
	function tampil_barang(){
		$hsl=$this->db->query("SELECT * FROM barang");
		return $hsl;
	}

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($ID)
    {
        return $this->db->get_where($this->_table, ["ID" => $ID])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->ID = uniqid();
        $this->Kode_Barang = $post["Kode_Barang"];
        $this->Barcode = $post["Barcode"];
        $this->Nama_Barang = $post["Nama_Barang"];
		$this->Satuan = $post["Satuan"];
	    $this->Harga = $post["Harga"];
		$this->Created_Date = $post["Created_Date"];
		$this->Update_Date = $post["Update_Date"];
        $this->db->insert($this->Barang, $this);
    }

    public function update()
    {
        $this->ID = uniqid();
        $this->Kode_Barang = $post["Kode_Barang"];
        $this->Barcode = $post["Barcode"];
        $this->Nama_Barang = $post["Nama_Barang"];
		$this->Satuan = $post["Satuan"];
	    $this->Harga = $post["Harga"];
		$this->Created_Date = $post["Created_Date"];
		$this->Update_Date = $post["Update_Date"];
        $this->db->update($this->Barang, $this, array('ID' => $post['ID']));
    }

    public function delete($ID)
    {
        return $this->db->delete($this->_table, array("ID" => $ID));
    }
}