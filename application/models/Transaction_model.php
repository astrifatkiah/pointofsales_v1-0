<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model
{
    private $_table = "tbl_jual";

    public $jual_nofak;
    public $jual_tanggal;
    public $jual_total;
    public $jual_jml_uang;
    public $jual_kembalian;
	public $jual_user_id;
	public $jual_keterangan;
    public function rules()
    {
        return [
            ['field' => 'jual_nofak',
            'label' => 'Jual nofak',
            'rules' => 'required'],

            ['field' => 'jual_tanggal',
            'label' => 'Jual tanggal',
            'rules' => 'required'],
            
            ['field' => 'jual_total',
            'label' => 'Jual total',
            'rules' => 'required'],
			
			['field' => 'jual_jml_uang',
            'label' => 'Jual jml uang',
            'rules' => 'required'],
			
			['field' => 'jual_kembalian',
            'label' => 'Jual kembalian',
            'rules' => 'numeric'],
			
			['field' => 'jual_user_id',
	        'label' => 'Jual user id'],
            
			['field' => 'jual_keterangan',
            'label' => 'Jual keterangan']
            
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function get_nofak($nofak)
    {
        return $this->db->get_where($this->_table, ["jual_nofak" => $jual_nofak])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->jual_nofak = $post ["jual_nofak"];
        $this->jual_tanggal = $post["jual_tanggal"];
        $this->jual_total = $post["jual_total"];
        $this->jual_jml_uang = $post["jual_jml_uang"];
		$this->jual_kembalian = $post["jual_kembalian"];
	    $this->jual_user_id = $post["jual_user_id"];
		$this->jual_keterangan = $post["jual_keterangan"];
        $this->db->insert($this->tbl_jual, $this);
    }

    public function update()
    {
         $this->jual_nofak = $post ["jual_nofak"];
        $this->jual_tanggal = $post["jual_tanggal"];
        $this->jual_total = $post["jual_total"];
        $this->jual_jml_uang = $post["jual_jml_uang"];
		$this->jual_kembalian = $post["jual_kembalian"];
	    $this->jual_user_id = $post["jual_user_id"];
		$this->jual_keterangan = $post["jual_keterangan"];
        $this->db->update($this->tbl_jual, $this, array('jual_nofak' => $post['jual_nofak']));
    }

    public function delete($ID)
    {
        return $this->db->delete($this->_table, array("jual_nofak" => $jual_nofak));
    }
}