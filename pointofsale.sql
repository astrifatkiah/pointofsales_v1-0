CREATE DATABASE pointofsales;
USE pointofsales;

CREATE TABLE Barang(
ID INT NOT NULL AUTO_INCREMENT,
Kode_Barang VARCHAR(5),
Barcode VARCHAR(50),
Nama_Barang VARCHAR(100),
Satuan VARCHAR(20),
Harga INT, 
Created_Date DATETIME,
Update_Date  DATETIME,
PRIMARY KEY(ID)
);
SELECT * FROM Barang
ALTER TABLE Barang 
MODIFY Created_Date 


UPDATE Barang SET Kode_Barang='B0001',Barcode='12345678',Nama_Barang='Indomie',Satuan='pcs', Harga='12000', Update_Date=NOW() WHERE ID='2' 

CREATE TABLE Satuan(
ID INT NOT NULL AUTO_INCREMENT,
Kode_Satuan VARCHAR(5),
Deskripsi VARCHAR(25),
Created_Date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
Update_Date  DATETIME,
PRIMARY KEY(ID)
);
 SELECT * FROM Satuan
 
 ALTER TABLE `pointofsales`.`satuan`   
  CHANGE `Created_Date` `Created_Date` DATETIME
  
  CREATE TABLE Pelanggan(
  ID INT NOT NULL AUTO_INCREMENT,
  Kode_Pelanggan VARCHAR(5),
  Nama VARCHAR(50),
  Alamat VARCHAR(100),
  Tempat VARCHAR(50),
  Tanggal_Lahir DATETIME,
  Jenis_Kelamin VARCHAR(15),
  Pekerjaan VARCHAR(50),
  Created_Date DATETIME,
  Update_Date DATETIME,
  PRIMARY KEY(ID)
  );
  
SELECT * FROM Pelanggan

  
